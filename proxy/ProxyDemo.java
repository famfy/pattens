package proxy;

public class ProxyDemo {

	public static void main(String[] args) {
		Subject sub = new Proxy();
		sub.publicMethod();
	}

}
