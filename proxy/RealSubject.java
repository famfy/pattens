package proxy;

public class RealSubject implements Subject{

	@Override
	public void publicMethod() {
		System.out.println("The real Object prints here...");
		doSomething();
	}
	
	private void doSomething(){
		
	}
	
	private void doSomething1(){
		
	}

}
