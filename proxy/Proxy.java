package proxy;

public class Proxy implements Subject{

	private RealSubject real;
	
	public Proxy(){
		real = new RealSubject();
	}
	
	@Override
	public void publicMethod() {
		// TODO Auto-generated method stub
		real.publicMethod();
	}

}
