package proxy;

public interface Subject {
	void publicMethod();
}
