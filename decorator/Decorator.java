package decorator;


abstract class Decorator implements Widget {
 // "has a" relationship
 private Widget widget;

 public Decorator(Widget widget) {
     this.widget = widget;
 }

 // Delegation
 public void draw() {
     widget.draw();
     //
 }
}

