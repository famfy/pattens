package decorator;

public class DecoratorDemo {
    public static void main(String[] args) {
        // Client has the responsibility to compose desired configurations
        Widget widget = new BorderDecorator(
        		//new BorderDecorator(
        				new ScrollDecorator(
        						new TextField(80, 24)));
        Widget widget1 = new ScrollDecorator(
				new TextField(80, 24));
        widget.draw();
    }
}
