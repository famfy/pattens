package FactoryMethod;

public class FactoryMethodMain {

	public static void main(String[] args) {
		Logger myLoggerFile = LoggerFactory.getLogger("file");
		Logger myLoggerDB = LoggerFactory.getLogger("db");
		Logger myLoggerNet = LoggerFactory.getLogger("net");
		Logger myLoggerNull = LoggerFactory.getLogger("fake");
		
		myLoggerFile.LogIt();
		myLoggerDB.LogIt();
		myLoggerNet.LogIt();
	}
}
