package FactoryMethod;

public class LoggerFactory
{
    public static Logger getLogger(String type)
    {
    	Logger logger = null;
        switch (type)
        {
            case "file":
            	logger = new FileLogger();
                break;
            case "db":
            	logger = new DBLogger();
                break;
            case "net":
            	logger = new NetLogger();
                break;
        }
        return (logger);
    }
}

