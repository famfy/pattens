package iterator;

public class IteratorDemo {
	public static void main(String[] args) {
		IteratorCollection recordCollection = new IteratorCollection();
		IIterator iter = recordCollection.createIterator();
 
		while(iter.hasNext()){
			System.out.println(iter.next());
		}	
	}
}
