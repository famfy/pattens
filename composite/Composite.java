import java.util.ArrayList;
import java.util.List;

public class Composite implements Graphic {
	private List<Graphic> childs = new ArrayList<Graphic>();
	
	@Override
	public void draw() {
		for (Graphic item : childs) {
			item.draw();
        }
	}
	
	public void add(Graphic newItem){
		childs.add(newItem);
	}
	
	public void remove(Graphic item){
		childs.remove(item);
	}

}
