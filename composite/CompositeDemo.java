

// java.awt.Component (Interface) -> awt.Container (Composite)

public class CompositeDemo {
	public static void main(String[] args) {
		Graphic elipse1 = new Ellipse();
		Graphic elipse2 = new Ellipse();
		
		Composite composite1 = new Composite();
		composite1.add(elipse1);
		composite1.add(elipse2);
		
		Composite composite2 = new Composite();
		composite2.add(elipse1);
		composite2.add(elipse2);
		
		Graphic elipse3 = new Ellipse();
		Graphic rect1 = new Rectangle();
		composite2.add(elipse3);
		composite2.add(rect1);
		
		composite1.add(composite2);
		
		composite1.draw();
		
		
	}
}
